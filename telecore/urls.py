from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from telecore.views import Test

urlpatterns = [
    path('', csrf_exempt(Test.as_view()))


]
