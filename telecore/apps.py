from django.apps import AppConfig


class TelecoreConfig(AppConfig):
    name = 'telecore'
