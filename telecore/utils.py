from telecore.models import UsersModel


def add_user(data):
    if not UsersModel.objects.filter(user_id=data.chat.id).exists():
        UsersModel.objects.create(user_id=data.chat.id,
                                  first_name=data.chat.first_name, last_name=data.chat.last_name)

# def add_city(data):
#     item = UsersModel.objects.filter(user_id=data.chat.id)
#     #item.update(city=)
