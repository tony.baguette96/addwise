from django.db import models
from django.db.models import Model, CharField, DateTimeField


# Create your models here.

class UsersModel(Model):
    user_id = CharField(max_length=11, db_index=True)
    first_name = CharField(max_length=120)
    last_name = CharField(max_length=120)
    date = DateTimeField(auto_now_add=True)
    city = CharField(max_length=120, blank=True)
    age = CharField(max_length=11, db_index=True, blank=True)
