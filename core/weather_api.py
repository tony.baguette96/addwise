from copy import copy

import requests


class WeatherApi:
    def __init__(self):
        self.api_url = 'http://api.openweathermap.org/data/2.5/find'
        self.app_id = '89db4cafa7622bcef1b7383d5979e1b0'
        self.params = {'q': None, 'type': 'like', 'units': 'metric', 'lang': 'ru', 'APPID': self.app_id}

    def get_weather(self, city):
        params = copy(self.params)
        params['q'] = city
        res = requests.get(self.api_url, params=params)
        data = res.json()
        temperature = str(data['list'][0]['main']['temp']) + ' градусов, и, похоже ' \
                      + (data['list'][-1]['weather'][0]['description'])
        return temperature


weather_api = WeatherApi()
# print(weather_api.get_weather('Moscow'))
