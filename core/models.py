from django.db import models

# Create your models here.
from django.db.models import Model, IntegerField, CharField, ForeignKey, CASCADE


class CityModal(Model):
    id_city = IntegerField()
    name = CharField(max_length=120, db_index=True)
    country = CharField(max_length=120, blank=True)

class CityVariations(Model):
    id_city = ForeignKey(CityModal, on_delete=CASCADE, related_name='variation')
    variation = CharField(max_length=120, db_index=True)


