

@bot.message_handler(commands=['start'])
def send_welcome(tdata):
    # bot.send_message(message.chat.id, text='Привет, меня зовут AddWise.')
    # get_city(message)
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    button_geo = types.KeyboardButton(text="Отправить местоположение", request_location=True)
    keyboard.add(button_geo)
    bot.send_message(tdata.message.chat.id,
                     'Чтобы я мог быть полезнее, введите название города в котором находитесь',
                     reply_markup=keyboard)
    bot.send_message(tdata.message.chat.id, text='Your location is' + tdata.message.location)


def weather_forecast(request):
    s_city = 'Moscow,RU'
    appid = '89db4cafa7622bcef1b7383d5979e1b0'
    res = requests.get("http://api.openweathermap.org/data/2.5/find",
                       params={'q': s_city, 'type': 'like', 'units': 'metric', 'lang': 'ru', 'APPID': appid})
    data = res.json()
    temperature = str(data['list'][0]['main']['temp']) + ', ' + (data['list'][-1]['weather'][0]['description'])
    return temperature